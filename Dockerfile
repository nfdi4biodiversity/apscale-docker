#FROM python:slim
FROM python@sha256:ca78039cbd3772addb9179953bbf8fe71b50d4824b192e901d312720f5902b22
LABEL org.opencontainers.image.authors="Martin Zurowietz <martin@cebitec.uni-bielefeld.de>"

RUN apt-get update \
    && apt-get install --no-install-recommends -y build-essential \
    && MAKEFLAGS="-j$(nproc)" pip install --no-cache-dir apscale \
    && apt-get purge -y build-essential \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/*

ARG VSEARCH_VERSION="2.21.1"
RUN apt-get update \
    && apt-get install --no-install-recommends -y \
        curl \
        zlib1g-dev \
        libbz2-dev \
    && cd /tmp \
    && curl -LO https://github.com/torognes/vsearch/releases/download/v$VSEARCH_VERSION/vsearch-$VSEARCH_VERSION-linux-x86_64.tar.gz \
    && tar xzf vsearch-$VSEARCH_VERSION-linux-x86_64.tar.gz \
    && mv vsearch-$VSEARCH_VERSION-linux-x86_64/bin/vsearch /usr/local/bin \
    && mv vsearch-$VSEARCH_VERSION-linux-x86_64/man/vsearch.1 /usr/local/bin \
    && cd / \
    && rm -r /tmp/vsearch-* \
    && apt-get purge -y curl \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -r /var/lib/apt/lists/*

# Disable Python output buffering.
ENV PYTHONUNBUFFERED=1

RUN mkdir /workspace

WORKDIR /workspace

ENTRYPOINT ["apscale"]

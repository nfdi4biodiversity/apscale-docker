# apscale-docker

[apscale](https://github.com/DominikBuchner/apscale) in a Docker container.

## Build

Clone this repository and run: `docker build -t apscale .`

## Run

Create a new project called "NAME":

```
docker run --rm -v $PWD:/workspace apscale --create_project NAME
```

Run analysis on the current project (you must be inside the project directory):

```
docker run --rm -v $PWD:/workspace apscale --run_apscale
```
